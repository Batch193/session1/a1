package Activity;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner entryScanner = new Scanner(System.in);

        System.out.println("First Name: ");
        firstName = entryScanner.nextLine().trim();
        System.out.println("Last Name: ");
        lastName = entryScanner.nextLine().trim();
        System.out.println("First Subject Grade: ");
        firstSubject = entryScanner.nextDouble();
        System.out.println("Second Subject Grade: ");
        secondSubject = entryScanner.nextDouble();
        System.out.println("Third Subject Grade: ");
        thirdSubject = entryScanner.nextDouble();
        System.out.println("Good day, " + firstName + lastName);
        double averageGrade = (firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Your grade average is: " + averageGrade);

    }

}
